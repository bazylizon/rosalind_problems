library("Biostrings")
library(tidyverse)
library(PTXQC)


s <- readDNAStringSet("rosalind_lcsm.fasta")

seq_name <- names(s)
sequance <- paste(s)

df <- data.frame(seq_name, sequance)


df <- df %>% 
  mutate(sequance = as.character(sequance))


vec <- df$sequance

LCSn(vec)
