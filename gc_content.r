library("Biostrings")
library(tidyverse)



s <- readDNAStringSet("rosalind_gc.fasta")

seq_name <- names(s)
sequance <- paste(s)

df <- data.frame(seq_name, sequance)

df %>% view()


df <- df %>% 
  mutate(sequance = as.character(sequance)) %>% 
  mutate(GC_ammount = str_count(sequance, "G") + str_count(sequance, "C")) %>% 
  mutate(len = str_length(sequance)) %>% 
  mutate(gc_content = GC_ammount/len*100)


df %>% 
  arrange(desc(gc_content)) %>% 
  select(seq_name, gc_content)
